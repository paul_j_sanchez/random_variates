#### DESCRIPTION

This gem implements random variate generation for several
common statistical distributions.  Each distribution is implemented
in its own class, and different parameterizations are created as
instances of the class using the constructor to specify the
parameterization.  All constructors use named parameters for clarity,
so the order of parameters does not matter. All random variate classes
provide an optional argument `rng`, with which the user can specify a
pseudo-random number generator that has a `rand` method that provides
U(0,1) values to use as the core source of randomness.
If `rng` is not specified, it defaults to an instance of
class `Xoroshiro::Random`.

Once a random variate class has been instantiated, values can either be
generated on demand using the `next` method (preferred for speed) or by
creating an `Enumerator` for use in any iterable context. For example,
the following will create an array of fifty exponentially distributed
values having rate 3.

    my_exp = RV::Exponential.new(rate: 3)
    my_exp.each.take(50)

---

#### RELEASE NOTES

**v0.5**

Prior releases focused on statistical correctness and adding distributions. This
release is about speed improvements. Substantial speedups have been accomplished by:

 - replacing the prior U(0,1) `Enumerator` architecture with direct calls to the PRNG;
 - replacing `loop do` with the measurably faster `while true` in
   acceptance/rejection based algorithms; and
 - replacing ruby's built-in `Random` with `Xoroshiro::Random` as the default
   PRNG. Xoshiro256** is faster than ruby's MT19937 implementation, and passes
   all tests in the TestU01 suite. See [xoshiro / xoroshiro generators and the
   PRNG shootout](https://prng.di.unimi.it) for more information.

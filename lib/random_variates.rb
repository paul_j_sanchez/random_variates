# frozen_string_literal: true

require 'xoroshiro'

# This library implements random variate generation for several
# common statistical distributions.  Each distribution is implemented
# in its own class, and different parameterizations are created as
# instances of the class using the constructor to specify the
# parameterization.  All constructors use named parameters for clarity,
# so the order of parameters does not matter. All RV classes provide an
# optional argument +rng+, with which the user can specify a U(0,1)
# PRNG object to use as the core source of randomness.  If +rng+ is
# not specified, it defaults to +Kernel#rand+.
#
# Once a random variate class has been instantiated, values can either be
# generated on demand using the +next+ method or by using the instance as
# a generator in any iterable context.

module RV
  # Set default PRNG.
  U_GENERATOR = Xoroshiro::Random.new

  # The +RV_Generator+ module provides a common core of methods to make
  # all of the RV classes iterable.
  module RV_Generator
    def next
      raise 'next not implemented!'
    end

    def each
      Enumerator.new do |y|
        while true
          y << self.next
        end
      end
    end
  end

  # Generate values uniformly distributed between +min+ and +max+.
  #
  # *Arguments*::
  #   - +min+ -> the lower bound for the range (default: 0).
  #   - +max+ -> the upper bound for the range (default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Uniform
    include RV_Generator

    attr_reader :min, :max

    def initialize(min: 0.0, max: 1.0, rng: U_GENERATOR)
      raise 'Max must be greater than min.' unless max > min
      @min = min
      @max = max
      @rng = rng
    end

    def next
      @rng.rand(@min..@max)
    end
  end

  # Triangular random variate generator with specified +min+, +mode+, and +max+.
  #
  # *Arguments*::
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #   - +min+ -> the lower bound for the range.
  #   - +max+ -> the upper bound for the range.
  #   - +mode+ -> the highest likelihood value (+min+ ≤ +mode+ ≤ +max+).
  #   - +mean+ -> the expected value of the distribution.
  #
  class Triangle
    include RV_Generator

    attr_reader :min, :max, :mode, :range, :mean

    def initialize(rng: U_GENERATOR, **args)
      param_names = %i[mean min max mode]
      unless args.size > 2 && args.keys.all? { |k| param_names.include? k }
        raise "invalid args - can only be #{param_names.join ', '}, or rng."
      end

      param_names.each { |k| args[k] ||= nil } if args.size < param_names.size

      nil_args = args.select { |_, v| v.nil? }.keys
      nil_args_count = nil_args.count
      raise 'too many nil args' if nil_args_count > 1

      args.transform_values! &:to_f

      if nil_args_count == 0
        if args[:mean] != (args[:min] + args[:max] + args[:mode]) / 3.0
          raise 'inconsistent args'
        end
      else
        key = nil_args.shift
        case key
        when :mean
          args[key] = (args[:min] + args[:max] + args[:mode]) / 3.0
        else
          others = param_names - [key, :mean]
          args[key] = 3 * args[:mean] - args.values_at(*others).sum
        end
      end

      param_names.each { |parm| instance_variable_set("@#{parm}", args[parm]) }

      @rng = rng
      @range = @max - @min
      raise 'Min must be less than Max.' if @range <= 0
      unless (@min..@max).include? @mode
        raise 'Mode must be between Min and Max.'
      end
      @crossover_p = (@mode - @min).to_f / @range
    end

    def next
      u = @rng.rand
      u < @crossover_p ?
        @min + Math.sqrt(@range * (@mode - @min) * u) :
        @max - Math.sqrt(@range * (@max - @mode) * (1.0 - u))
    end
  end

  # Exponential random variate generator with specified +rate+ or +mean+.
  # One and only one of +rate+ or +mean+ should be specified.
  #
  # *Arguments*::
  #   - +rate+ -> the rate of occurrences per unit time (default: +nil+).
  #   - +mean+ -> the expected value of the distribution (default: +nil+).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Exponential
    include RV_Generator

    attr_reader :rate, :mean

    def initialize(rate: nil, mean: nil, rng: U_GENERATOR)
      raise 'Rate must be positive.' if rate && rate <= 0
      raise 'Mean must be positive.' if mean && mean <= 0
      unless rate.nil? ^ mean.nil?
        raise 'Supply one and only one of mean or rate'
      end

      if rate.nil?
        @mean = mean
        @rate = 1.0 / mean
      else
        @mean = 1.0 / rate
        @rate = rate
      end
      @rng = rng
    end

    def next
      -@mean * Math.log(@rng.rand)
    end

    def rate=(rate)
      raise 'Rate must be a number.' unless rate.is_a? Numeric
      raise 'Rate must be positive.' if rate <= 0
      @mean = 1.0 / rate
      @rate = rate
    end

    def mean=(mean)
      raise 'Mean must be a number.' unless mean.is_a? Numeric
      raise 'Mean must be positive.' if mean <= 0
      @mean = mean
      @rate = 1.0 / mean
    end
  end

  # Normal/Gaussian random variate generator with specified
  # +mean+ and +standard deviation+.  Defaults to a standard normal.
  #
  # *Arguments*::
  #   - +mu+ -> the expected value (default: 0).
  #   - +sigma+ -> the standard deviation (default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Normal
    include RV_Generator
    BOUND = 2.0 * Math.sqrt(2.0 / Math::E)

    attr_reader :mu, :sigma

    def initialize(mu: 0.0, sigma: 1.0, rng: U_GENERATOR)
      raise 'Standard deviation must be positive.' if sigma <= 0

      @mu = mu
      @sigma = sigma
      @rng = rng
    end

    def next
      while true
        u = @rng.rand
        next if u == 0.0
        v = BOUND * (@rng.rand - 0.5)
        x = v / u
        x_sqr = x * x
        u_sqr = u * u
        if 6.0 * x_sqr <= 44.0 - 72.0 * u + 36.0 * u_sqr - 8.0 * u * u_sqr
          return @sigma * x + @mu
        elsif x_sqr * u >= 2.0 - 2.0 * u_sqr
          next
        elsif x_sqr <= -4.0 * Math.log(u)
          return @sigma * x + @mu
        end
      end
    end

    def sigma=(sigma)
      raise 'sigma must be a number.' unless sigma.is_a? Numeric
      @sigma = sigma
    end

    def mu=(mu)
      raise 'mu must be a number.' unless mu.is_a? Numeric
      @mu = mu
    end
  end

  # Alternate normal/Gaussian random variate generator with specified
  # +mean+ and +standard deviation+.  Defaults to a standard normal.
  #
  # *Arguments*::
  #   - +mu+ -> the expected value (default: 0).
  #   - +sigma+ -> the standard deviation (default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class BoxMuller
    include RV_Generator
    TWO_PI = 2.0 * Math::PI

    attr_reader :mu, :sigma

    def initialize(mu: 0.0, sigma: 1.0, rng: U_GENERATOR)
      raise 'Standard deviation must be positive.' if sigma <= 0

      @mu = mu
      @sigma = sigma
      @rng = rng
      @need_new_pair = false
    end

    def next
      if @need_new_pair ^= true
        theta = TWO_PI * @rng.rand
        d = @sigma * Math.sqrt(-2.0 * Math.log(@rng.rand))
        @next_norm = @mu + d * Math.sin(theta)
        @mu + d * Math.cos(theta)
      else
        @next_norm
      end
    end

    def sigma=(sigma)
      raise 'sigma must be a number.' unless sigma.is_a? Numeric
      @sigma = sigma
    end

    def mu=(mu)
      raise 'mu must be a number.' unless mu.is_a? Numeric
      @mu = mu
    end
  end

  # Gamma generator based on Marsaglia and Tsang method Algorithm 4.33
  #
  # Produces gamma RVs with expected value +alpha+ * +beta+.
  #
  # *Arguments*::
  #   - +alpha+ -> the shape parameter (+alpha+ > 0; default: 1).
  #   - +beta+ -> the rate parameter (+beta+ > 0; default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Gamma
    include RV_Generator

    attr_reader :alpha, :beta

    def initialize(alpha: 1.0, beta: 1.0, rng: U_GENERATOR)
      raise 'Alpha and beta must be positive.' if alpha <= 0 || beta <= 0

      @alpha = alpha
      @beta = beta
      @rng = rng
      @std_normal = Normal.new(rng: rng)
    end

    def next
      __gen__(@alpha, @beta)
    end

    private

    def __gen__(alpha, beta)
      if alpha > 1
        z = v = 0.0
        d = alpha - 1.0 / 3.0
        c = (1.0 / 3.0) / Math.sqrt(d)
        while true
          while true
            z = @std_normal.next
            v = 1.0 + c * z
            break if v > 0
          end
          z2 = z * z
          v = v * v * v
          u = @rng.rand
          break if u < 1.0 - 0.0331 * z2 * z2
          break if Math.log(u) < (0.5 * z2 + d * (1.0 - v + Math.log(v)))
        end
        d * v * beta
      else
        __gen__(alpha + 1.0, beta) * (@rng.rand**(1.0 / alpha))
      end
    end
  end

  # Weibull generator based on Devroye
  #
  # *Arguments*::
  #   - +rate+ -> the scale parameter (+rate+ > 0; default: 1).
  #   - +k+ -> the shape parameter (+k+ > 0; default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Weibull
    include RV_Generator

    attr_reader :rate, :k

    def initialize(rate: 1.0, k: 1.0, rng: U_GENERATOR)
      raise 'Rate and k must be positive.' if rate <= 0 || k <= 0

      @rate = rate
      @k = k
      @rng = rng
      @power = 1.0 / k
    end

    def next
      (-Math.log(@rng.rand))**@power / @rate
    end
  end

  # Erlang generator - Weibull restricted to integer +k+
  #
  # *Arguments*::
  #   - +rate+ -> the scale parameter (+rate+ > 0; default: 1).
  #   - +k+ -> the shape parameter (+k+ > 0; default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Erlang < Weibull
    def initialize(rate: 1.0, k: 1, rng: U_GENERATOR)
      raise 'K must be integer.' unless k.integer?

      super(rate: rate, k: k, rng: rng)
    end
  end

  # von Mises generator.
  #
  # This von Mises distribution generator is based on the VML algorithm by
  # L. Barabesis: "Generating von Mises variates by the Ratio-of-Uniforms Method"
  # Statistica Applicata Vol. 7, #4, 1995
  # http://sa-ijas.stat.unipd.it/sites/sa-ijas.stat.unipd.it/files/417-426.pdf
  #
  # *Arguments*::
  #   - +kappa+ -> concentration coefficient (+kappa+ ≥ 0).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class VonMises
    include RV_Generator

    attr_reader :kappa

    def initialize(kappa:, rng: U_GENERATOR)
      raise 'kappa must be positive.' if kappa < 0
      @kappa = kappa
      @rng = rng
      @s = (kappa > 1.3 ? 1.0 / Math.sqrt(kappa) : Math::PI * Math.exp(-kappa))
    end

    def next
      while true
        r1 = @rng.rand
        theta = @s * (2.0 * @rng.rand - 1.0) / r1
        next if theta.abs > Math::PI
        return theta if (0.25 * @kappa * theta * theta < 1.0 - r1) ||
                        (0.5 * @kappa * (Math.cos(theta) - 1.0) >= Math.log(r1))
      end
    end
  end

  # Poisson generator.
  #
  # *Arguments*::
  #   - +rate+ -> expected number per unit time/distance (+rate+ > 0; default: 1).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Poisson
    include RV_Generator

    attr_reader :rate

    def initialize(rate: 1.0, rng: U_GENERATOR)
      raise 'rate must be positive.' if rate <= 0

      @rate = rate
      @threshold = Math.exp(-rate)
      @rng = rng
    end

    def rate=(rate)
      raise 'Rate must be a number.' unless rate.is_a? Numeric
      raise 'Rate must be positive.' if rate <= 0
      @rate = rate
      @threshold = Math.exp(-rate)
    end

    def next
      count = 0
      product = 1.0
      count += 1 until (product *= @rng.rand) < @threshold
      count
    end
  end

  # Geometric generator.  Number of trials until first "success".
  #
  # *Arguments*::
  #   - +p+ -> the probability of success (0 < +p+ < 1; default: 0.5).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Geometric
    include RV_Generator

    attr_reader :p

    def initialize(p: 0.5, rng: U_GENERATOR)
      raise 'Require 0 < p < 1.' if p <= 0 || p >= 1

      @log_q = Math.log(1 - p)
      @rng = rng
    end

    def next
      (Math.log(1.0 - @rng.rand) / @log_q).ceil
    end
  end

  # Binomial generator.  Number of "successes" in +n+ independent trials.
  #
  # *Arguments*::
  #   - +n+ -> the number of trials (+p+ > 0, integer; default: 1).
  #   - +p+ -> the probability of success (0 < +p+ < 1; default: 0.5).
  #   - +rng+ -> the (+Enumerable+) source of U(0, 1)'s (default: U_GENERATOR)
  #
  class Binomial
    include RV_Generator

    attr_reader :n, :p

    def initialize(n: 1, p: 0.5, rng: U_GENERATOR)
      raise 'N must be an integer.' unless n.is_a? Integer
      raise 'N must be positive.' if n <= 0
      raise 'Require 0 < p < 1.' if p <= 0 || p >= 1

      @n = n.to_i
      @p = p
      @complement = false
      if @p <= 0.5
        @log_q = Math.log(1 - p)
      else
        @log_q = Math.log(@p)
        @complement = true
      end
      @rng = rng
    end

    def next
      result = sum = 0
      while true
        sum += Math.log(@rng.rand) / (@n - result)
        break if sum < @log_q
        result += 1
      end
      @complement ? @n - result : result
    end
  end
end

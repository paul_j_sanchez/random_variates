# -*- ruby -*-
_VERSION = "0.5.0"

Gem::Specification.new do |s|
  s.name = "random_variates"
  s.version = _VERSION
  s.date = "2023-05-25"
  s.summary = "Random variate generator classes for popular distributions."
  s.homepage = "https://bitbucket.org/paul_j_sanchez/random_variates.git"
  s.email = "pjs@alum.mit.edu"
  s.description = "Random variate generators implemented with next and Enumerator."
  s.author = "Paul J Sanchez"
  s.require_paths = ['lib']
  s.files = %w[
    README.md
    LICENSE.md
    lib/random_variates.rb
  ]
  s.metadata = {
    'rubygems_mfa_required' => 'true',
    'homepage_uri' => s.homepage,
    'documentation_uri' => 'https://rubydoc.info/gems/random_variates'
  }
  s.add_runtime_dependency 'xoroshiro', '~> 0.2'
  s.required_ruby_version = '>= 3.0'
  s.license = 'MIT'
end

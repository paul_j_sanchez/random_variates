#!/usr/bin/env ruby

require 'xoroshiro'
require 'benchmark/ips'

require_relative '../lib/random_variates'

N = 1_000

mt = Random.new
x = Xoroshiro::Random.new

def run_next_with(rng, n = N)
  distributions = [
    RV::Uniform.new(min: 1, max: 10, rng: rng),
    RV::Triangle.new(min:0, max: 5, mode: 4, rng: rng),
    RV::Normal.new(mu: 0.0, sigma: 1.0, rng: rng),
    RV::BoxMuller.new(mu: 0.0, sigma: 1.0, rng: rng),
    RV::Weibull.new(rate: 1.0 / 2.0, k: 1.5, rng: rng),
    RV::Weibull.new(rate: 1.0 / 2.0, k: 5, rng: rng),
    RV::Gamma.new(alpha: 0.5, beta: 5, rng: rng),
    RV::Gamma.new(alpha: 0.8, beta: 4, rng: rng),
    RV::Erlang.new(rate: 3, k: 5, rng: rng),
    RV::Erlang.new(rate: 5, k: 3, rng: rng),
    RV::Geometric.new(p: 0.5, rng: rng),
    RV::Geometric.new(p: 0.1, rng: rng),
    RV::Poisson.new(rate: 1, rng: rng),
    RV::Poisson.new(rate: 10, rng: rng),
    RV::Binomial.new(n: 10, p: 0.2, rng: rng),
    RV::Binomial.new(n: 10, p: 0.8, rng: rng)
  ]
  distributions.each { |d| n.times { d.next } }
end

def run_take_with(rng, n = N)
  distributions = [
    RV::Uniform.new(min: 1, max: 10, rng: rng),
    RV::Triangle.new(min:0, max: 5, mode: 4, rng: rng),
    RV::Normal.new(mu: 0.0, sigma: 1.0, rng: rng),
    RV::BoxMuller.new(mu: 0.0, sigma: 1.0, rng: rng),
    RV::Weibull.new(rate: 1.0 / 2.0, k: 1.5, rng: rng),
    RV::Weibull.new(rate: 1.0 / 2.0, k: 5, rng: rng),
    RV::Gamma.new(alpha: 0.5, beta: 5, rng: rng),
    RV::Gamma.new(alpha: 0.8, beta: 4, rng: rng),
    RV::Erlang.new(rate: 3, k: 5, rng: rng),
    RV::Erlang.new(rate: 5, k: 3, rng: rng),
    RV::Geometric.new(p: 0.5, rng: rng),
    RV::Geometric.new(p: 0.1, rng: rng),
    RV::Poisson.new(rate: 1, rng: rng),
    RV::Poisson.new(rate: 10, rng: rng),
    RV::Binomial.new(n: 10, p: 0.2, rng: rng),
    RV::Binomial.new(n: 10, p: 0.8, rng: rng)
  ]
  distributions.each { |d| d.each.take(n) }
end

puts "\n=== Speed comparisons of Random's rand vs Xoroshiro's rand ==="
puts
Benchmark.ips do |b|
  b.report('rand take:') { run_take_with(mt) }
  b.report('rand next:') { run_next_with(mt) }
  b.report('x take:') { run_take_with(x) }
  b.report('x next:') { run_next_with(x) }
  b.compare!
end

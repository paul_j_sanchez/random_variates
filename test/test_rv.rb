#!/usr/bin/env ruby -w

require_relative '../lib/random_variates.rb'
require 'xoroshiro'

u_sources = [
  Enumerator.new do |y|
    x = Xoroshiro::Random.new(seed: 12_345)
    loop { y.yield x.rand }
  end.lazy,
  Xoroshiro::Random.new(seed: 12_345).each.lazy
]

n = (ARGV.shift || 5).to_i
# print 'Normal,weibull1,weibull2,gamma1,gamma2,erlang1,erlang2,'
# puts 'geom3,geom4,poiss1,poiss2'

u_sources.each do |rng|
  distributions = [
    RV::Uniform.new(min: 1, max: 10, rng: rng),
    RV::Triangle.new(min:0, max: 5, mode: 4, rng: rng),
    RV::Normal.new(mu: 0.0, sigma: 1.0, rng: rng),
    RV::BoxMuller.new(mu: 0.0, sigma: 1.0, rng: rng),
    RV::Weibull.new(rate: 1.0 / 2.0, k: 1.5, rng: rng),
    RV::Weibull.new(rate: 1.0 / 2.0, k: 5, rng: rng),
    RV::Gamma.new(alpha: 0.5, beta: 5, rng: rng),
    RV::Gamma.new(alpha: 0.8, beta: 4, rng: rng),
    RV::Erlang.new(rate: 3, k: 5, rng: rng),
    RV::Erlang.new(rate: 5, k: 3, rng: rng),
    RV::Geometric.new(p: 0.5, rng: rng),
    RV::Geometric.new(p: 0.1, rng: rng),
    RV::Poisson.new(rate: 1, rng: rng),
    RV::Poisson.new(rate: 10, rng: rng),
    RV::Binomial.new(n: 10, p: 0.2, rng: rng),
    RV::Binomial.new(n: 10, p: 0.8, rng: rng)
  ]
  distributions.each { |d| puts "#{d.class}: #{d.next}" }
  distributions.each { |d| puts "#{d.class}: " + d.each.first(n).join(', ') }
end

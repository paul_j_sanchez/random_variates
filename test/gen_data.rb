#!/usr/bin/env ruby

require_relative '../lib/random_variates.rb'

n = (ARGV.shift || 5).to_i

distributions = [
  RV::Uniform.new(min: 1, max: 10),
  RV::Triangle.new(min:0, max: 5, mode: 4),
  RV::Exponential.new(mean: 5.0),
  RV::Normal.new(mu: 0.0, sigma: 1.0),
  RV::BoxMuller.new(mu: 0.0, sigma: 1.0),
  RV::Gamma.new(alpha: 0.5, beta: 5),
  RV::Gamma.new(alpha: 0.8, beta: 4),
  RV::Weibull.new(rate: 1.0 / 2.0, k: 1.5),
  RV::Weibull.new(rate: 1.0 / 2.0, k: 5),
  RV::Erlang.new(rate: 3, k: 5),
  RV::Erlang.new(rate: 5, k: 3),
  RV::VonMises.new(kappa: 2.5),
  RV::Poisson.new(rate: 1),
  RV::Poisson.new(rate: 10),
  RV::Geometric.new(p: 0.5),
  RV::Geometric.new(p: 0.1),
  RV::Binomial.new(n: 10, p: 0.2)
]
data = Array.new(distributions.size) do |i|
  [distributions[i].class] + distributions[i].each.take(n)
end.transpose
data.each { |line| puts line.join(',') }
